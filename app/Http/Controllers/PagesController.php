<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class PagesController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(2);
        return view('pages.index',compact('posts'));
    }
    
    public function show(Post $post)
    {
        // dd($post);
        return view('pages.show',compact('post'));
    }

    //
       public function contact()
    {
        return view('pages.contact');
    }
    //
       public function about()
    {
        return view('pages.about');
    }
    //
}
