<?php

namespace App\Http\Controllers;
use App\DataTables\PostsDatatable;
use Illuminate\Http\Request;
use App\Models\Post;
use Helper;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // public function index()
     // {
     //     $posts = Post::with('user')->get(); 
     //     return view('front.post.index',compact('posts'));
     // } 


     public function index(PostsDatatable $post)
    {

        return $post->render('backend.posts.index',['title'=>'posts controll']);
    }

    public function Posts_get_data_table()
   {
      return PostsDatatable::eloquent(Post::query())->make(true);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug_title = str_slug($request->title, '-');
        Post::create([
            'title'=> $request->title,
            'slug'=> $slug_title,
            'content'=> $request->content,
            'status'=> $request->status,
            'user_id'=> $request->user_id,]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('backend.posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
       return view('backend.posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $post = Post::find($id);
     $slug_title = str_slug($request->title, '-');
         $post->title = $request->title;
         $post->slug = $slug_title;
         $post->content = $request->content;
         $post->status = $request->status;
         $post->user_id = $request->user_id;
         $post->save();
     // return redirect()->back();

     return redirect()->route('posts.show', [$post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $redirect = true)
    {
        $post = Post::findOrFail($id);
        if (file_exists(public_path('uploads/' . $post->image))) {
            @unlink(public_path('uploads/' . $post->image));
        }
        $post->delete();

        if ($redirect) {
            session()->flash('success', trans('main.deleted-message'));
            return redirect()->route('posts.index');
        }
    }


    public function multi_delete(Request $request)
    {
        if (count($request->selected_data)) {
            foreach ($request->selected_data as $id) {
                $this->destroy($id, false);
            }
            session()->flash('success', trans('main.deleted-message'));
            return redirect()->route('posts.index');
        }
    }
}
