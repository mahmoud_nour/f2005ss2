<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Message;
use DB;


class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::first();
      $messages = User::find(Auth()->user()->id)->messages()->
      where('user_id', Auth()->user()->id)->
      Orwhere('receivermsgid', Auth()->user()->id)->get();
      // $inbox = User::find(Auth()->user()->id)->messages()->get();
      $inbox = DB::table('messages')
                    ->where('receivermsgid',Auth()->user()->id)
                    ->get();
      
      // dd($inbox);
        return view('backend.messages.index',compact('users','messages','inbox' )
      );
    }
    public function create()
    {
        $users = User::all();
        return view('backend.messages.create_message',compact('users'));
    }

    public function store(Request $request){
        $requestAll = $request->all();
      // $msg_from = auth()->user()->id;
      $message = Message::create($requestAll);
      // $receivermsgid = $request->input('receivermsgid');
      // $subject = $request->input('subject');
      // $msg_body = $request->input('msg_body');
      // dd($msg_body);
      // dd($request->all());
      return redirect()->route('messages.index');
    }
    public function show($id){
        $message = Message::findOrFail($id);
        if ($message->user_id !==auth()->user()->id ) {
          $message->msg_status = 1;
          $message->save();
        }
        $message_from = DB::table('users')
                    ->where('id', $message->user_id)
                    ->first();
        return view('backend.messages.show',compact('message','message_from'));
    }
    public function edit($id){

    }
    public function update(Request $request, $id)
    {
      $user = Message::find($id);
    }
    public function destroy($id, $redirect = true)
    {
        $message = Message::findOrFail($id);
        $message->delete();

        if ($redirect) {
            session()->flash('success', trans('main.deleted-message'));
            return redirect()->route('messages.index');
        }

    }
}
