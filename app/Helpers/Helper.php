<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;

class Helper
{

    /**
     * Uploads The File
     *
     * @param string $dir
     * @param UploadedFile $image
     * @param string $checkFunction
     * @return string
     */
    public static function Upload(string $dir, UploadedFile $image, string $checkFunction = null) : string
    {
        return UploadImages($dir, $image, $checkFunction);
    }

    /**
     * Upload Update
     *
     * @param string $oldFile the old file that will be unlinked
     * @param string $dir
     * @param UploadedFile $image the new file
     * @param string $checkFunction
     * @return string
     */
    public static function UploadUpdate(string $oldFile, string $dir, UploadedFile $image, string $checkFunction = null) : string
    {
        if (!empty($oldFile) && !is_null($oldFile) && file_exists(public_path('uploads/' . $oldFile))) {
            @unlink(public_path('uploads/' . $oldFile));
        }
        return UploadImages($dir, $image, $checkFunction);
    }
}
