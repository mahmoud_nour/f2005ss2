<?php

Route::get('/','PagesController@index')->name('index');
Route::get('{post}','PagesController@show')->name('index.show');

Route::get('contact','PagesController@contact')->name('contact_us');
Route::get('about','PagesController@about')->name('about_us');

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
