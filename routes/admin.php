<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use App\Http\Controllers\DataTables;
Route::middleware(\App\Http\Middleware\LangMiddleware::class)->group(function () {


    // what did you mean by this....
    // thy just you did'nt make it resource ?? oky

// Messages
    Route::get('/messages', 'MessagesController@index')->name('messages.index');
    Route::get('/messages/create', 'MessagesController@create')->name('messages.create');
    Route::get('/messages/show/{message}', 'MessagesController@show')->name('messages.show');

    Route::post('/messages/create', 'MessagesController@store')->name('messages.store');

    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/logout', 'AdminController@logout')->name('admin.logout');
    Route::get('/lang/{lang}', 'AdminController@changeLang')->name('admin.changeLang');

    // users
    Route::resource('users', 'UsersController');
    Route::post('users/multi_delete', 'UsersController@multi_delete')->name('users.multi_delete');

    // users
    Route::resource('posts', 'PostsController');
    Route::post('posts/multi_delete', 'PostsController@multi_delete')->name('posts.multi_delete');

    // roles and permissions
    Route::resource('roles', 'RoleController');
    Route::post('/roles/multi_delete', 'RoleController@multi_delete')->name('roles.multi_delete');

    Route::resource('permissions', 'PermissionController');
    Route::post('permissions/multi_delete', 'PermissionController@multi_delete')->name('permissions.multi_delete');

    // Routes Goes Here
});
