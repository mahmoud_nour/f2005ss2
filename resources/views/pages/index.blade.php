@extends('front.layout.app')
@section('content')
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                {{-- {{dd($posts)}} --}}
                @foreach($posts as $post)
                    <article>
                        <div class="post-video">
                            <div class="post-heading">
                                <h3><a href="#">
                                {{$post->title}}
                                </a></h3>
                            </div>
                        </div>
                        <p>
                           {{$post->content}}
                        </p>
                        <div class="bottom-article">
                            <ul class="meta-post">
                                <li><i class="icon-calendar"></i>
                                <a href="#"> 
                                {{$toDate = date('d-M-y', strtotime($post->created_at))}}
                                </a></li>
                                <li><i class="icon-user"></i><a href="#"> 
                                {{$post->user->name}}
                                </a></li>
                            </ul>
                            <a href="{{ route('index.show',$post->id) }}" class="pull-right">Continue reading <i class="icon-angle-right"></i></a>
                        </div>
                    </article>
                @endforeach

                    <div id="pagination">
                        {{ $posts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
