@extends('front.layout.app')
@section('content')
    <section id="content">
        <div class="container">
            <div class="row"><div class="row"> 
                    <div class="col-lg-2">
                        
                    </div>
                    <div class="col-lg-8">
                        <h4>{{$post->title}}</h4>
                        {{-- <img src="/front/img/dummies/dummy-1.jpg" alt="" class="align-left"> --}}
                        <p>
                         {{$post->content}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
