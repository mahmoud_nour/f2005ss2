<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>numberspq</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" numberspq " />
    <!-- css -->
    <link href="front/css/bootstrap.min.css" rel="stylesheet" />
    <link href="front/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="front/css/jcarousel.css" rel="stylesheet" />
    <link href="front/css/flexslider.css" rel="stylesheet" />
    <link href="front/css/style.css" rel="stylesheet" />
    <!-- Theme skin -->
    <link href="front/skins/default.css" rel="stylesheet" />

</head>

<body>
    <div id="wrapper">
        <!-- start header -->
        <header>
            <div class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="{{ route('index') }}"><span>N</span>umberspq</a>
                    </div>
                    <div class="navbar-collapse collapse ">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="{{ route('index') }}">Home</a></li>
                            @guest
                            <li class="dropdown ">
                                <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">login / register<b class=" icon-angle-down"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('login') }}">login</a></li>
                                    <li><a href="{{ route('register') }}">register</a></li>
                                </ul>
                            </li>
                            @else
                            <li class="dropdown ">
                                <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">{{auth::user()->name}}<b class=" icon-angle-down"></b></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                            
                            <li><a href="{{ route('about_us') }}">About</a></li>
                            <li><a href="{{ route('contact_us') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

            @yield('content')
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="widget">
                            <h5 class="widgetheading">Get in touch with us</h5>
                            <address>
                                <strong>numberspq</strong><br>
                                address 
                            </address>
                            <p>
                                <i class="icon-phone"></i> (123) 456-7890 - (123) 456-7891 <br>
                                <i class="icon-envelope-alt"></i> email@numberspq.com
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget">
                            <h5 class="widgetheading">Pages</h5>
                            <ul class="link-list">
                                <li><a href="#">Terms and conditions</a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Career center</a></li>
                                <li><a href="#">Contact us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                        </div>
                        <div class="col-lg-6">
                            <ul class="social-network">
                                <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="front/js/jquery.js"></script>
    <script src="front/js/jquery.easing.1.3.js"></script>
    <script src="front/js/bootstrap.min.js"></script>
    <script src="front/js/jquery.fancybox.pack.js"></script>
    <script src="front/js/jquery.fancybox-media.js"></script>
    <script src="front/js/google-code-prettify/prettify.js"></script>
    <script src="front/js/portfolio/jquery.quicksand.js"></script>
    <script src="front/js/portfolio/setting.js"></script>
    <script src="front/js/jquery.flexslider.js"></script>
    <script src="front/js/animate.js"></script>
    <script src="front/js/custom.js"></script>

</body>

</html>
