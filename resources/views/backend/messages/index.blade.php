@extends('backend.theme.layout.app')

@section('styles')
    @include('backend.users.incs._styles')
    <link rel="stylesheet" href="{{asset('backend/messages/style.css')}}">
@endsection

@section('content')
  <span class="caption-subject bold uppercase font-blue">{{ $title = 'show messagese'}}</span>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                  <div class="container">
	                   <div class="row">
		                    <section class="content">
                  			<div class="col-md-8">
                  				<div class="panel panel-default">
                  					<div class="panel-body">
                  						<div class="pull-right">
                  							<div class="btn-group">
                  								{{-- <button type="button" class="btn btn-success btn-filter" data-target="pagado">read</button> --}}
                  								{{-- <button type="button" class="btn btn-warning btn-filter" data-target="pendiente">unread</button> --}}
                  								<button type="button" class="btn btn-default btn-filter" data-target="all">all</button>
                                  <button type="button" class="btn btn-danger btn-filter" data-target="cancelado">inbox</button>


                                </div>
                  						</div>
                  						<div class="table-container">
                  							<table class="table table-filter">
                  								<tbody>
                                    @foreach ($messages as $message)
                                      {{-- {{dd(auth()->user()->id)}} --}}
                                      {{-- {{dd($message->user_id)}} --}}
                  									{{-- <tr data-status="{{($message->msg_status == '') ? 'pendiente':'pagado'}}"> --}}
                  									<tr data-status=
                                    @if ($message->receivermsgid == auth()->user()->id)
                                      "cancelado"
                                    @endif
                                    @if ($message->msg_status == '')
                                      "pendiente"
                                    @elseif ($message->msg_status == 1)
                                      "pagado"
                                    @endif

                                    >

                  										<td>
                  											<div class="media">
                                          <a href="{{ route('messages.show',$message->id) }}" style="text-decoration: none;" target="_blank" class="pull-left">
                                            <div class="media-body">
                                              <span class="media-meta pull-right">Febrero 13, 2016</span>
                                              <h4 class="title">
                                                {{$message->msg_subject}}
                                                @if ($message->msg_status == Null)
                                                  <span class="pull-right pendiente">(unread)</span>
                                                  @elseif ($message->receivermsgid == auth()->user()->id)
                                                  {{-- <span class="pull-right cancelado danger">(inbox)</span> --}}
                                                  {{-- <span class="pull-right pagado" style="color: #36c6d3">(read)</span> --}}
                                                @elseif ($message->msg_status == 1)
                                                    <span class="pull-right pagado" style="color: #36c6d3">(read)</span>
                                                @endif
                                              </h4>
                                              {{-- {{dd($message->msg_status)}} --}}
                                              <p class="summary">{{$message->msg_body}}</p>
                                            </div>
                                          </a
                  											</div>
                  										</td>
                                    </tr>
                                    @endforeach
                  								</tbody>
                  							</table>
                  						</div>
                  					</div>
                  				</div>
                  				<div class="content-footer">
                  				</div>
                  			</div>
		                   </section>
	                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{asset('backend/messages/style.js')}}"></script>
@endsection
