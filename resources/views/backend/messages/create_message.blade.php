@extends('backend.theme.layout.app')

@section('styles')
    @include('backend.users.incs._styles')
@endsection

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                      <span class="caption-subject bold uppercase font-blue">{{ $title = 'create message'}}</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="" data-toggle="tooltip" title="{{ trans('main.show-all') }}   {{ trans('main.users') }}"> <i class="fa fa-list"></i> </a>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form method="post" action="{{ route('messages.store') }}" class="form-horizontal" role="form" enctype="multipart/form-data">
                        @csrf
                      <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">

                          <label class="col-md-2 control-label">subject <span class="required"></span> </label>

                          <div class="col-md-6">
                              <input type="text" name="msg_subject"   class="form-control" placeholder="subject" required>

                              @if ($errors->has('msg_subject'))
                                  <span class="help-block">
                                      <strong class="help-block">{{ $errors->first('msg_subject') }}</strong>
                                  </span>

                              @endif
                          </div>
                      </div>
                      <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                          <label class="col-md-2 control-label">select type<span class="required"></span> </label>
                          <div class="col-md-6">
                              <select class="form-control"  name="msg_type">
                                  <option value="">‬</option>
                                  <option value="1">‫استفسار‬</option>
                                  <option value="5">حل مشكله عضويه</option>
                                  <option value="6">تجديد الاشتراك‬</option>
                                  <option value="2">اشتراك‬</option>
                                  <option value="3">ملاحظه</option>
                                  <option value="4">‫شكر‬</option>
                              </select>
                              @if ($errors->has('type'))
                                  <span class="help-block">
                                      <strong class="help-block">{{ $errors->first('type') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                          <label class="col-md-2 control-label">select user<span class="required"></span> </label>
                          <div class="col-md-6">
                              <select class="form-control"  name="receivermsgid">
                                  <option value=""></option>
                                  @foreach ($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                  @endforeach
                              </select>
                              @if ($errors->has('type'))
                                  <span class="help-block">
                                      <strong class="help-block">{{ $errors->first('type') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                      <div class="form-group">
                        <label for="Textarea1">message</label>
                        <textarea name="msg_body" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                      </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn green">{{ trans('main.add') }} {{ trans('main.user') }}</button>
                                    <a href="" class="btn default">{{ trans('main.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
