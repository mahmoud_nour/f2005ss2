@extends('backend.theme.layout.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('backend/datatables/dataTables.bootstrap-rtl.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-blue-casablanca uppercase"> {{ $title = 'all posts' }} </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- {!! $dataTable->table(['class'=> 'table table-striped table-bordered table-hover'], true) !!} --}}
<table id='users-table' class="table">
  <thead>
      <tr>
        <td>id</td>
          <td>title</td>
          <td>content</td>
          <td>status</td>
          {{-- <td>user_id</td> --}}
          <td>created_at</td>
          <td>updated_at</td>
          <td>edit</td>
          <td>delete</td>
      </tr>
  </thead>
</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  <script src="{{asset('backend/datatables/dataTables.min.js')}}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>
  <script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
  <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
  <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

  <script type="text/javascript">
      $(function() {
          $('#users-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: 'http://127.0.0.1:8000/admin/Posts/Posts_get_data_table',
              columns:[
                {data:'id'},
                {data:'title'},
                {data:'content'},
                {data:'status'},
                // {data:'user_id'},
                {data:'created_at'},
                {data:'updated_at'},
              ]
          });
      });
  </script>
@endsection
