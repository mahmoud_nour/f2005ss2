<div class="form-body">
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label class="col-md-2 control-label">{{ trans('main.title') }} <span class="required"></span> </label>
        <div class="col-md-6">
            <input type="text" name="title" value="{{ getData($data, 'title') }}" class="form-control" placeholder="{{ trans('main.title') }}" required>

            @if ($errors->has('title'))
                <span class="help-block">
                    <strong class="help-block">{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
        <label class="col-md-2 control-label">post status<span class="required"></span> </label>
        <div class="col-md-6">
            <select class="form-control" id="status" name="status">
                <option value=""></option>
                <option value="1" {{ getData($data, 'status') == '1' ? ' selected' : '' }}>{{trans('main.publish')}}</option>
                <option value="0" {{ getData($data, 'status') == '0' ? ' selected' : '' }}>{{trans('main.unpublish')}}</option>
            </select>
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong class="help-block">{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
    <div class="form-group">
      <label for="Textarea1">content</label>
      <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="5">{{ getData($data, 'content') }}</textarea>
    </div>
 
</div>
