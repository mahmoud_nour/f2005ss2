@extends('backend.theme.layout.app')

@section('styles')
    @include('backend.users.incs._styles')
    <link rel="stylesheet" href="{{asset('backend/messages/style.css')}}">
@endsection

@section('content')
  <span class="caption-subject bold uppercase font-blue">{{ $title = $post->title }}</span>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                  <div class="container">
	                   <div class="row">
		                    <section class="content">
                  			<div class="col-md-8">
                          <div class="pull-left">
                            {{$post->created_at}}
                          </div>
                          <hr>
                              <div class="panel panel-default">
                                <div class="panel-heading">

                                  {{$post->title}}
                                </div>
                                <div class="panel-body">
                                  {{$post->content}}
                                </div>
                              </div>
                          <hr>
                        </div>
		                   </section>
	                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{asset('backend/messages/style.js')}}"></script>
@endsection
