@extends('backend.theme.layout.app')

@section('styles')
    @include('backend.users.incs._styles')
@endsection

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                      <span class="caption-subject bold uppercase font-blue">{{ $title = 'create post'}}</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="" data-toggle="tooltip" title="{{ trans('main.show-all') }}   {{ trans('main.users') }}"> <i class="fa fa-list"></i> </a>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form method="post" action="{{ route('posts.store') }}" class="form-horizontal" role="form" enctype="multipart/form-data">
                        @csrf
                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">

                          <label class="col-md-2 control-label">title <span class="required"></span> 
                          </label>

                          <div class="col-md-6">
                              <input type="text" name="title"   class="form-control" placeholder="title" required>

                              @if ($errors->has('type'))
                                  <span class="help-block">
                                      <strong class="help-block">{{ $errors->first('title') }}</strong>
                                  </span>

                              @endif
                          </div>
                      </div>
                      <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                          <label class="col-md-2 control-label">post status<span class="required"></span> </label>
                          <div class="col-md-6">
                              <select class="form-control"  name="status">
                                  <option value=""></option>
                                    <option value="1">publish</option>
                                    <option value="0">unpublish</option>
                              </select>
                              @if ($errors->has('status'))
                                  <span class="help-block">
                                      <strong class="help-block">{{ $errors->first('status') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                      <div class="form-group">
                        <label for="Textarea1">content</label>
                        <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                      </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn green">{{ trans('main.add') }} {{ trans('main.post') }}</button>
                                    <a href="" class="btn default">{{ trans('main.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
